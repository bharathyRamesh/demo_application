const con = require('../config/config');


var salesInfo =  function() {
    return new Promise((resolve, reject) => {
        value = con.getConnection().then((connection) => {
                connection.beginTransaction((err)=>{
                    if(err) {
                        connection.rollback(()=>{
                            connection.release();
                        })
                        reject(err);
                    } else {
                        var selectQuery = "select sales_person.id, sales_person.name, sales_person.date_of_joining, sales_person.mobile_no, product.name AS product_name, product.id AS product_id, product.price FROM purchase join  sales_person on purchase.saleman_id = sales_person.id join product on purchase.product_id = product.id"

                        connection.query(selectQuery,(err, result)=>{
                            if(err) {
                                console.log(err);
                                connection.rollback(()=>{
                                    connection.release();
                                })      
                            } else {
                                resolve(result);
                                connection.commit(()=>{
                                    connection.release();
                                })
                            }
                        })
                    }
    
                })
        });
    }) 
}

exports.salesInfo = salesInfo;