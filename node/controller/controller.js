const service = require('../service/appService');

const express = require('express');

const bodyParser = require('body-parser');

const  app = express.Router();


app.use(bodyParser.json());



app.get("/salesInfo", (req, res) => {
    service.salesInfo().then((result) => {
        res.send(result);
    })
})

module.exports = app;