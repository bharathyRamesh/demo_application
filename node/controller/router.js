var express = require('express');
var app = express();

var controller = require('./controller');
app.use('/info', controller);

app.listen(8080 ,()=> console.log("controller is listening"));