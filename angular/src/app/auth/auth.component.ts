import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

    @ViewChild('container') container: ElementRef;

    constructor(private location: Location) { }

    onSignUp() {
        this.container.nativeElement.classList.add('right-panel-active');
        this.location.go('auth/signup');
    }

    onSignIn() {
        this.container.nativeElement.classList.remove('right-panel-active');
        this.location.go('auth/signinasdfa');
    }

    ngOnInit() {
        if (window.location.pathname === '/auth/signup') {
            this.container.nativeElement.classList.add('right-panel-active');
        }
    }
}
